package main

import (
	"fmt"

	"github.com/yryz/ds18b20"
	"os"
)

func readTemp() (float64, float64)  {
	var c float64
	sensors, err := ds18b20.Sensors()
	if err != nil {
		fmt.Printf("err: %s\n", err)
		os.Exit(1)
	}
	for _, sensor := range sensors {
		c, err = ds18b20.Temperature(sensor)
		if err != nil {
			fmt.Printf("err: %s\n", err)
			os.Exit(1)
		}
	}
	f := (c * 1.8) + 32
	return c, f

}

func main() {
	c, f := readTemp()
    fmt.Printf("C%.2f\nF%.2f\n", c, f)
}